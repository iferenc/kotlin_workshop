package coroutines

import java.lang.Thread.sleep
import kotlin.concurrent.thread

fun main(args: Array<String>) = threads(100_000)

fun threads(n: Int) {
    val threads = List(n) {
        thread {
            sleep(1000L)
            println(it)
        }
    }
    println(System.currentTimeMillis())
    threads.forEach { it.join() }
    println(System.currentTimeMillis())
}
